import { StyleSheet } from 'react-native';

const customStyles = StyleSheet.create({

    headerFooterStyle: {
      width: '100%',
      height: 45,
      backgroundColor: '#606070',
    },
  
    timeInput: {
      width: 40,
      height: 50,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
    },
  
    timeInputLM: {
      width: 40,
      height: 50,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginLeft: 50
    },
  
    adjInput: {
      width: 40,
      height: 50,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginRight: 15,
    },
  
    adjInputLM: {
      width: 40,
      height: 50,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginLeft: 50,
    },

    thumb: {
        backgroundColor: '#eaeaea',
        borderColor: '#9a9a9a',
        borderRadius: 2,
        borderWidth: 1,
        height: 20,
        width: 20,
    },
    track: {
        backgroundColor: 'white',
        borderColor: '#9a9a9a',
        borderRadius: 2,
        borderWidth: 1,
        height: 14,
        marginLeft: 10,
        marginRight: 10,
    },
    sliderContainer: {
      width: 300
    }
  
  });