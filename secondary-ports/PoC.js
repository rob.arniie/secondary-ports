import { StatusBar } from 'expo-status-bar';
import AppLoading from "expo-app-loading";
import { StyleSheet, Text, View, TextInput, ScrollView, Alert  } from 'react-native';
import { useFonts } from 'expo-font';
import React, { useEffect, useRef, SafeAreaProvider } from "react";
import { Tab, TabView } from '@rneui/themed';
import {Slider} from '@miblanchard/react-native-slider';
import { format, compareAsc } from 'date-fns'


export default function App() {

  //TIME
  const [showAdjustment, setShow] = React.useState(false);
  const [showSlider, setSlider] = React.useState(false);
  const [showFinalCalculation, setShowFinalCalculation] = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const [highWaterEarlyTimeHoursValue,sethighWaterEarlyTimeHoursValue] = React.useState('');
  const [highWaterEarlyTimeMinsValue,sethighWaterEarlyTimeMinsValue] = React.useState('');
  const [highWaterLateTimeHoursValue,sethighWaterLateTimeHoursValue] = React.useState('');
  const [highWaterLateTimeMinsValue,sethighWaterLateTimeMinsValue] = React.useState('');

  const [lowAdjustment,setlowAdjustmentValue] = React.useState('');
  const [highAdjustment,sethighAdjustmentsValue] = React.useState('');

  const [sliderHoursLowValue,setSliderHoursLowValue] = React.useState(null);
  const [sliderHoursHighValue,setSliderHoursHighValue] = React.useState(null);
  const [sliderHoursValue,setSliderHoursValue] = React.useState(null);
  const [sliderHoursCurrentValue,setSliderHoursCurrentValue] = React.useState(null);

  const [sliderMinsValue,setSliderMinsValue] = React.useState(null);
  const [sliderMinsCurrentValue,setSliderMinsCurrentValue] = React.useState(null);

  const [finalCalculation,setFinalCalculation] = React.useState('');
  const [finalAdjustedTime, setFinalAdjustedTime] = React.useState('');

  const ref = React.useRef();
  const ref_input1 = useRef();
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();
  const ref_input5 = useRef();
  const ref_input6 = useRef();

  const createTimeMismatchAlert = () =>
    Alert.alert('Time Validation Error', 'Your range is not 6 hours', [
      {text: 'OK'},
    ]);

  //HEIGHT
  const [lowHeight,setLowHeight] = React.useState('');
  const [highHeight,setHighHeight] = React.useState('');
  const [showHeightAdjustment, setHeightShow] = React.useState(false);
  const [lowHeightAdjustment,setlowHeightAdjustmentValue] = React.useState('');
  const [highHeightAdjustment,sethighHeightAdjustmentsValue] = React.useState('');
  const [showHeightSlider, setHeightSlider] = React.useState(false);
  const [sliderHeightLowValue,setSliderHeightLowValue] = React.useState(null);
  const [sliderHeightHighValue,setSliderHeightHighValue] = React.useState(null);
  const [sliderHeightValue,setSliderHeightValue] = React.useState(null);
  const [sliderHeightCurrentValue,setSliderHeightCurrentValue] = React.useState(null);
  const [finalHeightCalculation,setFinalHeightCalculation] = React.useState('');
  const [showFinalHeightCalculation, setShowHeightFinalCalculation] = React.useState(false);
  const [finalAdjustedHeight, setFinalAdjustedHeight] = React.useState('');


  let [fontsLoaded] = useFonts({
    'HelveticaNeue': require('./assets/HelveticaNeue-Medium.otf'),
  });
  if (!fontsLoaded) {
      return <AppLoading/>;
  }

  function zeroPad(value) {
    if (Math.floor( value) < 10) {
      return "0"+Math.floor(value);
    } else {
      return Math.floor(value)
    }
  }

  function showHideRange() {

    if ((highWaterEarlyTimeHoursValue) 
      && (highWaterEarlyTimeMinsValue) 
      && (highWaterLateTimeHoursValue) 
      && (highWaterLateTimeMinsValue))  {


      //See if the early time is later than the late time!

      console.log(typeof highWaterEarlyTimeHoursValue);
      var timeError = false;
      var reverseTime = false;
      var sliderRangeHigh = false;

      switch(highWaterEarlyTimeHoursValue) {
        case '18':
          console.log(highWaterLateTimeHoursValue);
          if((highWaterLateTimeHoursValue != '00') && (highWaterLateTimeHoursValue != '12')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '12') {
              var reverseTime = true;
              //make sliderRange 18,19,20,21,22,23,24 (24 becomes 00)
              var sliderRangeHigh = 18;
            }
          }
          break;
        case '19':
          if((highWaterLateTimeHoursValue != '01') && (highWaterLateTimeHoursValue != '13')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '13') {
              var reverseTime = true;
              var sliderRangeHigh = 19;
            }
          }
          break;
        case '20':
          if((highWaterLateTimeHoursValue != '02') && (highWaterLateTimeHoursValue != '14')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '14') {
              var reverseTime = true;
              var sliderRangeHigh = 20;
            }
          }
          break;
        case '21':
          if((highWaterLateTimeHoursValue != '03') && (highWaterLateTimeHoursValue != '15')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '15') {
              var reverseTime = true;
              var sliderRangeHigh = 21;
            }
          }
          break;
        case '22':
          if((highWaterLateTimeHoursValue != '04') && (highWaterLateTimeHoursValue != '16')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '16') {
              var reverseTime = true;
              var sliderRangeHigh = 22;
            }
          }
          break;
        case '23':
          if((highWaterLateTimeHoursValue != '05') && (highWaterLateTimeHoursValue != '17')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '17') {
              var reverseTime = true;
              var sliderRangeHigh = 23;
            }
          }
          break;
        case '00':
          if((highWaterLateTimeHoursValue != '06') && (highWaterLateTimeHoursValue != '18')) {
            createTimeMismatchAlert();
            var timeError = true;
          } else {
            if (highWaterLateTimeHoursValue == '18') {
              var reverseTime = true;
              var sliderRangeHigh = 24;
            }
          }
          break;
        case '01':
            if((highWaterLateTimeHoursValue != '07') && (highWaterLateTimeHoursValue != '19')) {
              createTimeMismatchAlert();
              var timeError = true;
            } else {
              if (highWaterLateTimeHoursValue == '19') {
                var reverseTime = true;
                var sliderRangeHigh = 25;
              }
            }
            break;
          case '02':
            if((highWaterLateTimeHoursValue != '08') && (highWaterLateTimeHoursValue != '20')) {
              createTimeMismatchAlert();
              var timeError = true;
            } else {
              if (highWaterLateTimeHoursValue == '20') {
                var reverseTime = true;
                var sliderRangeHigh = 26;
              }
            }
            break;
          case '03':
              if((highWaterLateTimeHoursValue != '09') && (highWaterLateTimeHoursValue != '21')) {
                createTimeMismatchAlert();
                var timeError = true;
              } else {
                if (highWaterLateTimeHoursValue == '21') {
                  var reverseTime = true;
                  var sliderRangeHigh = 27;
                }
              }
              break;
          case '04':
            if((highWaterLateTimeHoursValue != '10') && (highWaterLateTimeHoursValue != '22')) {
              createTimeMismatchAlert();
              var timeError = true;
            } else {
              if (highWaterLateTimeHoursValue == '22') {
                var reverseTime = true;
                var sliderRangeHigh = 28;
              }
            }
            break;
          case '05':
            if((highWaterLateTimeHoursValue != '11') && (highWaterLateTimeHoursValue != '23')) {
              createTimeMismatchAlert();
              var timeError = true;
            } else {
              if (highWaterLateTimeHoursValue == '23') {
                var reverseTime = true;
                var sliderRangeHigh = 29;
              }
            }
            break;
          case '06':
            if((highWaterLateTimeHoursValue != '12') && (highWaterLateTimeHoursValue != '00')) {
              createTimeMismatchAlert();
              var timeError = true;
            } else {
              if (highWaterLateTimeHoursValue == '00') {
                var reverseTime = true;
                var sliderRangeHigh = 6;
              }
            }
            break;
        
      }
      if (timeError) {
        sethighWaterEarlyTimeHoursValue("");
        sethighWaterEarlyTimeMinsValue("");
        sethighWaterLateTimeHoursValue("");
        sethighWaterLateTimeMinsValue("");
        ref_input1.current.focus();
      } else {

        //First check to see if late is earlier than early!
        
        if ((reverseTime) || (highWaterEarlyTimeHoursValue > highWaterLateTimeHoursValue)) {
          console.log("inverse");

          if (!reverseTime) {
            //check to see if the user has enterred more than 6 hours
            var minsDiff = ((highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue)-(highWaterLateTimeHoursValue+highWaterLateTimeMinsValue));
        
            if(minsDiff != 600){
              createTimeMismatchAlert();
              sethighWaterEarlyTimeHoursValue("");
              sethighWaterEarlyTimeMinsValue("");
              sethighWaterLateTimeHoursValue("");
              sethighWaterLateTimeMinsValue("");
              ref_input1.current.focus();
            } else {
              setSliderHoursLowValue(Number(highWaterLateTimeHoursValue));
              setSliderHoursValue(Number(highWaterLateTimeHoursValue));
              setSliderMinsValue(Number(highWaterLateTimeMinsValue)); 
              setSliderHoursCurrentValue(zeroPad(Number(highWaterLateTimeHoursValue)));
              setSliderMinsCurrentValue(zeroPad(Number(highWaterLateTimeMinsValue)));
              setSliderHoursHighValue(Number(highWaterEarlyTimeHoursValue));
              setShow(true);
            }
          } else {
            setSliderHoursLowValue(Number(highWaterLateTimeHoursValue));
            setSliderHoursValue(Number(highWaterLateTimeHoursValue));
            setSliderMinsValue(Number(highWaterLateTimeMinsValue));  
            setSliderHoursCurrentValue(zeroPad(Number(highWaterLateTimeHoursValue)));
            setSliderMinsCurrentValue(zeroPad(Number(highWaterLateTimeMinsValue)));    
            
            if (sliderRangeHigh) {
              setSliderHoursHighValue(sliderRangeHigh);
            } else {
              setSliderHoursHighValue(Number(highWaterEarlyTimeHoursValue));
            }
          }

          
        } else {

          //check to see if the user has enterred more than 6 hours
          var minsDiff = ((highWaterLateTimeHoursValue+highWaterLateTimeMinsValue)-(highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue));
        
          if(minsDiff != 600){
            createTimeMismatchAlert();
            sethighWaterEarlyTimeHoursValue("");
            sethighWaterEarlyTimeMinsValue("");
            sethighWaterLateTimeHoursValue("");
            sethighWaterLateTimeMinsValue("");
            ref_input1.current.focus();
          } else {
            setSliderHoursLowValue(Number(highWaterEarlyTimeHoursValue));
            setSliderHoursValue(Number(highWaterEarlyTimeHoursValue));
            setSliderMinsValue(Number(highWaterEarlyTimeMinsValue)); 
            setSliderHoursCurrentValue(zeroPad(Number(highWaterEarlyTimeHoursValue)));
            setSliderMinsCurrentValue(zeroPad(Number(highWaterEarlyTimeMinsValue)));
            setSliderHoursHighValue(Number(highWaterLateTimeHoursValue));
            setShow(true);
          }
        } 
      }
    }
  }

  function showHideHeightRange() {

    if ((lowHeight) 
      && (highHeight))  {


      if (lowHeight > highHeight) {
        
        setSliderHeightLowValue(Number(lowHeight));

        setSliderHeightValue(Number(lowHeight));

        setSliderHeightCurrentValue(Number(lowHeight));

        setSliderHeightHighValue(Number(highHeight));
       
      } else {
        setSliderHeightLowValue(Number(highHeight));

        setSliderHeightValue(Number(highHeight));
       
        setSliderHeightCurrentValue(Number(highHeight));
       
        setSliderHeightHighValue(Number(lowHeight));
       
        
      }
      setHeightShow(true);
    }

    
  }
  

  function sliderHourChange(value) {
    console.log(value);
    if (value >= 24) {
      value = value-24;
    } 
    setSliderHoursValue(Number(value));
    setSliderHoursCurrentValue(zeroPad(Number(value)));
    
    tryCalc(null);
  }

  function sliderMinsChange(value) {
    setSliderMinsValue(Number(value));
    setSliderMinsCurrentValue(zeroPad(Number(value)));
    tryCalc(null);
  }

  function sliderHeightChange(value) {
    setSliderHeightValue(Number(value));
    setSliderHeightCurrentValue(Number(value));
    tryHeightCalc(null);
  }

  function tryCalc(event) {
    if ((lowAdjustment) && (highAdjustment)) {
      setSlider(true);

      if (typeof sliderHoursCurrentValue != 'undefined') {
        setShowFinalCalculation(true);

        //Interpolation: Y = ( ( X - X1 )( Y2 - Y1) / ( X2 - X1) ) + Y1
        //( X - X1 )
        //console.log("X - 0900", highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue);
        var earlyConcat = highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue;

        if ((sliderHoursHighValue >= 24) && (sliderHoursCurrentValue <= 17)) {
          //adjustment will be wrong
    
          var hoursPadded = Math.floor(sliderHoursCurrentValue)+24;
        } else {

          if (Math.floor( sliderHoursCurrentValue) < 10) {
            var hoursPadded = "0"+Math.floor(sliderHoursCurrentValue);
          } else {
            var hoursPadded = Math.floor( sliderHoursCurrentValue);
          }
        }

        if (Math.floor( sliderMinsCurrentValue) < 10) {
          var minsPadded = "0"+Math.floor( sliderMinsCurrentValue);
        } else {
          var minsPadded = Math.floor( sliderMinsCurrentValue);
        }

        var hoursMinsRange = "" + hoursPadded+minsPadded;
       
        
        console.log("hoursMinsRange", hoursMinsRange);

        var firstCalc = Number(hoursMinsRange)-Number(earlyConcat);
        //console.log("231?", firstCalc);

        //( Y2 - Y1)
        var secCalc = Number(highAdjustment)-Number(lowAdjustment);
        //console.log("10?", secCalc);

        //( X - X1 )( Y2 - Y1)
        var firstPartCalc = firstCalc*secCalc;
        //console.log("2310?", firstPartCalc);

        //( X2 - X1) ) 
        //console.log("X2?", Number(highWaterLateTimeHoursValue+highWaterLateTimeMinsValue));
        //console.log("X1?", highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue);
        var laterTimeHM = Number(highWaterLateTimeHoursValue+highWaterLateTimeMinsValue);
        var earlierTimeHM = Number(highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue);
        var thirdCalc = laterTimeHM-earlierTimeHM;

        //console.log("600?", thirdCalc);

        //( X - X1 )( Y2 - Y1) / ( X2 - X1)
        var secPartCalc = firstPartCalc/thirdCalc;
        //console.log("3.85?", secPartCalc);

        //All+Y1
        var totalCalc = Number(lowAdjustment)+Number(secPartCalc);
        /*console.log("40?", lowAdjustment);
        console.log("3.85?", secPartCalc);
        console.log("43.85?", totalCalc);*/
      

        var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (2 || -1) + '})?');
        var truncated = totalCalc.toString().match(re)[0];

        setFinalCalculation(truncated);

        console.log("truncated", truncated);

        if (truncated<0) {

          var hourAdjustments = Math.floor(truncated/60);
          var adjustedHours = Math.floor(sliderHoursCurrentValue)+hourAdjustments;

          adjustedMins = Math.floor(truncated-(hourAdjustments*60));

          var overallMins = (Number(sliderMinsCurrentValue)+Number(adjustedMins));
          console.log("overallMins", overallMins);

          if (overallMins >= 60) {
            adjustedHours = adjustedHours+1;
            adjustMins = overallMins-60;
          } else {
            adjustMins = overallMins;
          }
        

          setFinalAdjustedTime(zeroPad(adjustedHours)+":"+zeroPad(adjustMins));
        } else {
          console.log("more than hour needed", truncated);

          var hourAdjustments = Math.floor(truncated/60);
          var adjustedHours = Math.floor(sliderHoursCurrentValue)+hourAdjustments;

          adjustedMins = Math.floor(truncated-(hourAdjustments*60));

          var overallMins = (Number(sliderMinsCurrentValue)+Number(adjustedMins));
          console.log("overallMins", overallMins);

          if (overallMins >= 60) {
            adjustedHours = adjustedHours+1;
            adjustMins = overallMins-60;
          } else {
            adjustMins = overallMins;
          }

          setFinalAdjustedTime(zeroPad(adjustedHours)+":"+zeroPad(adjustMins));
        } 
        
      }
    }
  }

  function tryHeightCalc(event) {
    if ((lowHeightAdjustment) && (highHeightAdjustment)) {

      setHeightSlider(true);
     
      console.log("height 4. Slider is now", sliderHeightCurrentValue);
      console.log("height 5. Slider is now", typeof sliderHeightCurrentValue)

      if (typeof sliderHeightCurrentValue != 'undefined') {
        setShowHeightFinalCalculation(true);

        //Interpolation: Y = ( ( X - X1 )( Y2 - Y1) / ( X2 - X1) ) + Y1
        //( X - X1 )

        /*console.log("X1 - 11", lowHeight);
        console.log("Y1 - -3.4", highHeightAdjustment);
        console.log("X2 - 8.1", highHeight);
        console.log("Y2 - -4.8", lowHeightAdjustment);
        console.log("X - 9.4", sliderHeightCurrentValue);*/

        var xx1 = sliderHeightCurrentValue - lowHeight;
        //console.log("xx1 - -1.6", xx1);
        var y2y1 = lowHeightAdjustment - highHeightAdjustment;
        //console.log("y2y1 - -1.4", y2y1);
        var x2x1 = highHeight - lowHeight;
        //console.log("x2x1 - -2.9", x2x1);

        //( X - X1 )( Y2 - Y1)
        var firstHeightPart = xx1*y2y1;
        console.log("2.24", firstHeightPart);
        //( X2 - X1)
        var secondHeightPart = x2x1;
        //( X - X1 )( Y2 - Y1) / ( X2 - X1)
        var thirdHeightPart = firstHeightPart/secondHeightPart;
        console.log("-0.77", thirdHeightPart);
        //All + Y1
        var fourthHeightPart = Number(thirdHeightPart)+Number(highHeightAdjustment);
        console.log("-4.1", fourthHeightPart);

        var totalHeightCalc = fourthHeightPart;
        
        var reHeight = new RegExp('^-?\\d+(?:\.\\d{0,' + (2 || -1) + '})?');
        var heightTruncated = totalHeightCalc.toString().match(reHeight)[0];

        setFinalHeightCalculation(heightTruncated);

        setFinalAdjustedHeight(Number(heightTruncated)+Number(sliderHeightCurrentValue));
      }
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <>
          <Text style={{color: 'white', width: '100%', backgroundColor: '#00c2fb', height: 100, paddingTop: 60, alignContent: 'center', textAlign: 'center', fontFamily: 'HelveticaNeue'}}>Secondary Port Calculator by iPowerboat</Text>
        </>
        <>
          <Tab
            value={index}
            onChange={(e) => setIndex(e)}
            indicatorStyle={{
              backgroundColor: 'white',
            }}
            variant="primary"
          >
            <Tab.Item
              title="Times"
              titleStyle={{ fontSize: 12 }}
              containerStyle={(active) => ({
                backgroundColor: active ? undefined : "black",
              })}
            />
            <Tab.Item
              title="Height"
              titleStyle={{ fontSize: 12 }}
              containerStyle={(active) => ({
                backgroundColor: active ? undefined : "black",
              })}
            />
          </Tab>

          <TabView value={index} onChange={setIndex} animationType="spring">
            <TabView.Item style={{height: 800, width: 400}}>
                <View>
                  <View style={{flexDirection: 'row', marginTop: 50}}>
                    <Text style={{marginLeft: 30, marginRight: 20, paddingTop: 13}}>Times:</Text>
                    <TextInput value={highWaterEarlyTimeHoursValue} ref={ref_input1} autoFocus={true} keyboardType="numeric" returnKeyType="done" style={mystyles.timeInput} id="highWaterEarlyTimeHours" maxLength={2} name="highWaterEarlyTimeHours" onChangeText={(value)=>{if (value.length === 2) {ref_input2.current.focus()};sethighWaterEarlyTimeHoursValue(value)}} onBlur={showHideRange} />
                    <Text style={{paddingTop: 15}}>:</Text>
                    <TextInput value={highWaterEarlyTimeMinsValue} ref={ref_input2} keyboardType="numeric" returnKeyType="done" style={mystyles.timeInput} id="highWaterEarlyTimeMins"  maxLength={2} name="highWaterEarlyTimeMins" onChangeText={(value)=>{if (value.length === 2) {ref_input3.current.focus()};sethighWaterEarlyTimeMinsValue(value)}} onBlur={showHideRange} />

                    <TextInput value={highWaterLateTimeHoursValue} ref={ref_input3} keyboardType="numeric" returnKeyType="done" style={mystyles.timeInputLM} id="highWaterLateTimeHours"  maxLength={2} name="highWaterLateTimeHours" onChangeText={(value)=>{if (value.length === 2) {ref_input4.current.focus()};sethighWaterLateTimeHoursValue(value)}} onBlur={showHideRange} />
                    <Text style={{paddingTop: 15}}>:</Text>
                    <TextInput value={highWaterLateTimeMinsValue} ref={ref_input4} keyboardType="numeric" returnKeyType="done" style={mystyles.timeInput} id="highWaterLateTimeMins"  maxLength={2} name="highWaterLateTimeMins" onChangeText={(value)=>sethighWaterLateTimeMinsValue(value)} onBlur={showHideRange} />
                  </View>
                  <>
                    {showAdjustment ? 
                    <View style={{flexDirection: 'row', marginTop: 50}}>
                      <Text style={{marginLeft: 30, marginRight: 20, paddingTop: 13}}>Adjustment:</Text>
                      <TextInput ref={ref_input5} id="lowAdjustment" keyboardType="numeric" returnKeyType="done" style={mystyles.adjInput} name="lowAdjustment" onChangeText={(value)=>setlowAdjustmentValue(value)}  onBlur={tryCalc} />
                      <TextInput ref={ref_input6} id="highAdjustment" keyboardType="numeric" returnKeyType="done" style={mystyles.adjInputLM} name="highAdjustment" onChangeText={(value)=>sethighAdjustmentsValue(value)}  onBlur={tryCalc} />
                    </View> : null}
                  </>
                  <>
                    {showSlider ?
                    <><Text style={{marginLeft: 30, marginRight: 20, paddingTop: 23}}>Set Exact Time</Text>
                    <Text style={{marginLeft: 40, paddingTop: 13}}>Hours</Text>
                    <Slider
                    animateTransitions
                    thumbStyle={customStyles6.thumb}
                    trackStyle={customStyles6.track}
                    step={'1'}
                    minimumValue={sliderHoursLowValue}
                    maximumValue={sliderHoursHighValue}
                    minimumTrackTintColor="#606070"
                    maximumTrackTintColor="#d3d3d3"
                    value={sliderHoursValue}
                    onValueChange={(value)=>sliderHourChange(value)}
                    
                  />
                  <Text style={{marginLeft: 30, marginRight: 20, paddingTop: 13}}>Mins</Text>
                    <Slider
                    animateTransitions
                    thumbStyle={customStyles6.thumb}
                    trackStyle={customStyles6.track}
                    minimumValue={0}
                    maximumValue={59}
                    minimumTrackTintColor="#606070"
                    maximumTrackTintColor="#d3d3d3"
                    value={sliderMinsValue}
                    onValueChange={(value)=>sliderMinsChange(value)}
                    
                  />
                  <Text style={ { fontWeight: 'bold', fontSize: 15, width: 370, height: 40, textAlign: 'center', marginLeft: 30, marginRight: 20, paddingTop: 13 } }>
                  Standard Port: {sliderHoursCurrentValue}:{sliderMinsCurrentValue}
                </Text></>: null}
                  </>
                  <>
                  {showFinalCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 80, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#6699cc' } }>
                  Adjustment: {Math.floor(finalCalculation)} mins </Text>: null}
                  </>
                  <>
                  {showFinalCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 120, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#4172a3' } }>Secondary Port:{'\n'}
                  {finalAdjustedTime} </Text>: null}
                  </>
                  
                  
                
                </View>
            </TabView.Item>
            <TabView.Item>
            <View>
                <View style={{flexDirection: 'row', marginTop: 50}}>
                  <Text style={{paddingTop: 13, width: 100, height: 50, color: 'black', marginLeft: 30, }}>Height:</Text>
                  <TextInput keyboardType="numeric" returnKeyType="done" style={mystyles.timeInput} id="lowHeight" name="lowHeight" onChangeText={(value)=>setLowHeight(value)} onBlur={showHideHeightRange} />
                  <Text style={{paddingTop: 15, height: 50, color: 'black'}}> m</Text>
                

                  <TextInput keyboardType="numeric" returnKeyType="done" style={mystyles.timeInputLM} id="highHeight"  name="highHeight" onChangeText={(value)=>setHighHeight(value)} onBlur={showHideHeightRange} />
                  <Text style={{paddingTop: 15, height: 50, color: 'black'}}> m</Text>
                
                </View>
              <>
                  {showHeightAdjustment ? 
                  <View style={{flexDirection: 'row', marginTop: 50, marginBottom: 30}}>
                    <Text style={{marginLeft: 30, width: 100, height: 50, marginTop: 20, color: 'black', paddingTop: 13}}>Adjustment:</Text>
                    <TextInput id="lowHeightAdjustment" keyboardType="numeric" returnKeyType="done" style={mystyles.adjInput} name="lowHeightAdjustment" onChangeText={(value)=>setlowHeightAdjustmentValue(value)}  onBlur={tryHeightCalc} />
                    <TextInput id="highHeightAdjustment" keyboardType="numeric" returnKeyType="done" style={mystyles.adjInputLM} name="highHeightAdjustment" onChangeText={(value)=>sethighHeightAdjustmentsValue(value)}  onBlur={tryHeightCalc} />
                  </View> : null}
                </>
                <>
                  {showHeightSlider ?
                  <><Text style={{marginLeft: 30, marginRight: 20, paddingTop: 23, marginTop: 30}}>Set Exact Height</Text>
                  
                  <Slider
                  animateTransitions
                  thumbStyle={customStyles6.thumb}
                  trackStyle={customStyles6.track}
                  step={'0.1'}
                  minimumValue={sliderHeightLowValue}
                  maximumValue={(sliderHeightHighValue)}
                  minimumTrackTintColor="#606070"
                  maximumTrackTintColor="#d3d3d3"
                  value={sliderHeightValue}
                  onValueChange={(value)=>sliderHeightChange(value)}
                  
                />
                <Text style={ { fontWeight: 'bold', fontSize: 15, width: 370, height: 40, textAlign: 'center', marginLeft: 30, marginRight: 20, paddingTop: 13 } }>
                Standard Port: {sliderHeightCurrentValue.toFixed(1)} m
              </Text></>: null}
                </>
                <>
                {showFinalHeightCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 80, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#6699cc' } }>
                {finalHeightCalculation} m </Text>: null}
                </>
                <>
                  {showFinalHeightCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 120, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#4172a3' } }>Secondary Port:{'\n'}
                  {finalAdjustedHeight.toFixed(2)} </Text>: null}
                </>
                
                
              
              </View>
            </TabView.Item>
          </TabView>
          </>
        </View>
      </SafeAreaView>
  );
}

const mystyles = StyleSheet.create({

  headerFooterStyle: {
    width: '100%',
    height: 45,
    backgroundColor: '#606070',
  },

  timeInput: {
    width: 40,
    height: 50,
    padding: 5,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
  },

  timeInputLM: {
    width: 40,
    height: 50,
    padding: 5,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: 50
  },

  adjInput: {
    width: 40,
    height: 50,
    padding: 5,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginRight: 15,
  },

  adjInputLM: {
    width: 40,
    height: 50,
    padding: 5,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: 50,
  },

});
export const customStyles6 = StyleSheet.create({
  thumb: {
      backgroundColor: '#eaeaea',
      borderColor: '#9a9a9a',
      borderRadius: 2,
      borderWidth: 1,
      height: 20,
      width: 20,
  },
  track: {
      backgroundColor: 'white',
      borderColor: '#9a9a9a',
      borderRadius: 2,
      borderWidth: 1,
      height: 14,
      marginLeft: 10,
      marginRight: 10,
  },
  sliderContainer: {
    width: 300
  }
});