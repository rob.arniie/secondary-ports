import { StatusBar } from 'expo-status-bar';
import AppLoading from "expo-app-loading";
import { StyleSheet, Text, View, TextInput, ScrollView, Alert, SafeAreaView  } from 'react-native';
import { useFonts } from 'expo-font';
import React, { useEffect, useRef, SafeAreaProvider } from "react";
import { Tab, TabView } from '@rneui/themed';
import {Slider} from '@miblanchard/react-native-slider';
import { format, differenceInHours } from 'date-fns'

/**
 * TODO - seperate css styles out
 * import customStyles from "./assets/css/styles"
 **/
export default function App() {

  //Declare all Time related constants/variables
  const [showAdjustment, setShow] = React.useState(false);
  const [showSlider, setSlider] = React.useState(false);
  const [showFinalCalculation, setShowFinalCalculation] = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const [highWaterEarlyTimeHoursValue,sethighWaterEarlyTimeHoursValue] = React.useState(null);
  const [highWaterEarlyTimeMinsValue,sethighWaterEarlyTimeMinsValue] = React.useState(null);
  const [highWaterLateTimeHoursValue,sethighWaterLateTimeHoursValue] = React.useState(null);
  const [highWaterLateTimeMinsValue,sethighWaterLateTimeMinsValue] = React.useState(null);

  const [lowAdjustment,setlowAdjustmentValue] = React.useState(false);
  const [highAdjustment,sethighAdjustmentsValue] = React.useState(false);

  const [sliderHoursLowValue,setSliderHoursLowValue] = React.useState(null);
  const [sliderHoursHighValue,setSliderHoursHighValue] = React.useState(null);
  const [sliderHoursValue,setSliderHoursValue] = React.useState(null);
  const [sliderHoursCurrentValue,setSliderHoursCurrentValue] = React.useState(null);

  const [sliderMinsValue,setSliderMinsValue] = React.useState(null);
  const [sliderMinsCurrentValue,setSliderMinsCurrentValue] = React.useState(null);

  const [finalCalculation,setFinalCalculation] = React.useState('');
  const [finalAdjustedTime, setFinalAdjustedTime] = React.useState('');

  const ref = React.useRef();
  const ref_input1 = useRef();
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();
  const ref_input5 = useRef();
  const ref_input6 = useRef();

  const createTimeMismatchAlert = () =>
    Alert.alert('Time Validation Error', 'Your range is not 6 hours', [
      {text: 'OK'},
    ]);


  //Declare all Height related constants/variables
  const [lowHeight,setLowHeight] = React.useState('');
  const [highHeight,setHighHeight] = React.useState('');
  const [showHeightAdjustment, setHeightShow] = React.useState(false);
  const [lowHeightAdjustment,setlowHeightAdjustmentValue] = React.useState('');
  const [highHeightAdjustment,sethighHeightAdjustmentsValue] = React.useState('');
  const [showHeightSlider, setHeightSlider] = React.useState(false);
  const [sliderHeightLowValue,setSliderHeightLowValue] = React.useState(null);
  const [sliderHeightHighValue,setSliderHeightHighValue] = React.useState(null);
  const [sliderHeightValue,setSliderHeightValue] = React.useState(null);
  const [sliderHeightCurrentValue,setSliderHeightCurrentValue] = React.useState(null);
  const [finalHeightCalculation,setFinalHeightCalculation] = React.useState('');
  const [showFinalHeightCalculation, setShowHeightFinalCalculation] = React.useState(false);
  const [finalAdjustedHeight, setFinalAdjustedHeight] = React.useState('');


  let [fontsLoaded] = useFonts({
    'HelveticaNeue': require('./assets/HelveticaNeue-Medium.otf'),
  });
  if (!fontsLoaded) {
      return <AppLoading/>;
  }

   /**
   * Pad Number with 0 if integer is less than 10
   **/
  function zeroPad(value) {
    if (Math.floor( value) < 10) {
      return "0"+Math.floor(value);
    } else {
      return Math.floor(value)
    }
  }

  /**
   * Secondary Ports have to be 6 hours apart to calculate, it could be 18 if time is reversed
   * Reminder: dateLeft is the later time
   * Returns: boolean (true if + or - 6 or 18 hours )
   **/
  function validateHourGap(dateLeft, dateRight){
    var hoursDifference = differenceInHours(
      dateLeft,
      dateRight,
      {roundingMethod: "ceil"}
    );
    //console.log("dateLeft", dateLeft);
    //console.log("dateRight", dateRight);
    //console.log("Hours Difference:", Math.abs(hoursDifference));
    if ((Math.abs(hoursDifference) == 6) || (Math.abs(hoursDifference) == 18)) {
      return true;
    } else {
      return false;
    }
  }

  function showTimeError() {
    createTimeMismatchAlert();
  }

  /**
   * Called to see if we can show Adjustment Range
   **/
  function showHideRange() {

    //First check values are set
    if ((highWaterEarlyTimeHoursValue) 
      && (highWaterEarlyTimeMinsValue) 
      && (highWaterLateTimeHoursValue) 
      && (highWaterLateTimeMinsValue))  {

        //Now check there is an explicit 6 hour gap
        var highWaterEarlyDate = new Date();
        highWaterEarlyDate.setHours(highWaterEarlyTimeHoursValue);
        highWaterEarlyDate.setMinutes(highWaterEarlyTimeMinsValue);

        var highWaterLateDate = new Date();
        highWaterLateDate.setHours(highWaterLateTimeHoursValue);
        highWaterLateDate.setMinutes(highWaterLateTimeMinsValue);
      
        if(!validateHourGap(highWaterEarlyDate, highWaterLateDate)) {
          showTimeError();

          //TODO: Not firing? why?
          sethighWaterEarlyTimeHoursValue("");
          sethighWaterEarlyTimeMinsValue("");
          sethighWaterLateTimeHoursValue("");
          sethighWaterLateTimeMinsValue("");
          ref_input1.current.focus();
          setShow(false);
          setShowFinalCalculation(false);
          console.log("--CLEARING--");
          return;
        }

        //Get Slider range ready and see if the early time is later than the late time!
        var reverseTime = false;
        var sliderRangeHigh = false;

        /*
        * We only need to check explicitly for 6pm to 6am otherwise we can assume same day
        * eg reversTime needs to be set
        */

        if (parseInt(highWaterEarlyTimeHoursValue) > parseInt(highWaterLateTimeHoursValue)) {
          reverseTime = true;
        }

        if ((parseInt(highWaterEarlyTimeHoursValue) >= 0) && parseInt(highWaterEarlyTimeHoursValue) <= 6) {
          if (highWaterLateTimeHoursValue == parseInt(highWaterEarlyTimeHoursValue)+18) {
            reverseTime = true;
            sliderRangeHigh = parseInt(highWaterEarlyTimeHoursValue)+24;
          }
        }
        
        if ((parseInt(highWaterEarlyTimeHoursValue) >= 18) && parseInt(highWaterEarlyTimeHoursValue) <= 23) {
          if (Math.abs(parseInt(highWaterEarlyTimeHoursValue)-parseInt(highWaterLateTimeHoursValue) == 6)) {
            reverseTime = true;
            sliderRangeHigh = parseInt(highWaterEarlyTimeHoursValue);
          } else if (Math.abs(parseInt(highWaterEarlyTimeHoursValue)-parseInt(highWaterLateTimeHoursValue) == 18)) {
            sliderRangeHigh = parseInt(highWaterLateTimeHoursValue)+24;
          }
        }

        console.log("Reverse Time?", reverseTime);
        console.log("SliderRangerHigh", sliderRangeHigh);

        if (reverseTime) {
          setSliderHoursLowValue(Number(highWaterLateTimeHoursValue));
          setSliderHoursValue(Number(highWaterLateTimeHoursValue));
          setSliderMinsValue(Number(highWaterLateTimeMinsValue));  
          setSliderHoursCurrentValue(zeroPad(Number(highWaterLateTimeHoursValue)));
          setSliderMinsCurrentValue(zeroPad(Number(highWaterLateTimeMinsValue))); 
          
          if (sliderRangeHigh) {
            setSliderHoursHighValue(sliderRangeHigh);
          } else {
            setSliderHoursHighValue(Number(highWaterEarlyTimeHoursValue));
          }

        } else {
          setSliderHoursLowValue(Number(highWaterEarlyTimeHoursValue));
          setSliderHoursValue(Number(highWaterEarlyTimeHoursValue));
          setSliderMinsValue(Number(highWaterEarlyTimeMinsValue));  
          setSliderHoursCurrentValue(zeroPad(Number(highWaterEarlyTimeHoursValue)));
          setSliderMinsCurrentValue(zeroPad(Number(highWaterEarlyTimeMinsValue)));   

          if (sliderRangeHigh) {
            setSliderHoursHighValue(sliderRangeHigh);
          } else {
            setSliderHoursHighValue(Number(highWaterLateTimeHoursValue));
          }
        } 
        setShow(true);
    }
  }

  function showHideHeightRange() {
    if ((lowHeight) 
      && (highHeight))  {

      if (lowHeight > highHeight) {
        setSliderHeightLowValue(Number(lowHeight));
        setSliderHeightValue(Number(lowHeight));
        setSliderHeightCurrentValue(Number(lowHeight));
        setSliderHeightHighValue(Number(highHeight));
      } else {
        setSliderHeightLowValue(Number(highHeight));
        setSliderHeightValue(Number(highHeight));
        setSliderHeightCurrentValue(Number(highHeight));
        setSliderHeightHighValue(Number(lowHeight));
      }
      setHeightShow(true);
    }
  }
  

  function sliderHourChange(value) {
    if (value >= 24) {
      value = value-24;
    } 
    setSliderHoursValue(Number(value));
    setSliderHoursCurrentValue(zeroPad(Number(value)));
    tryCalc(null);
  }

  function sliderMinsChange(value) {
    setSliderMinsValue(Number(value));
    setSliderMinsCurrentValue(zeroPad(Number(value)));
    tryCalc(null);
  }

  function sliderHeightChange(value) {
    setSliderHeightValue(Number(value));
    setSliderHeightCurrentValue(Number(value));
    tryHeightCalc(null);
  }

  function showHourMinSlider() {
    if ((lowAdjustment) && (highAdjustment)) {
      setSlider(true);
    }
  }

  function tryCalc(event) {
    

      if (typeof sliderHoursCurrentValue != 'undefined') {
        setShowFinalCalculation(true);

        //Interpolation: Y = ( ( X - X1 )( Y2 - Y1) / ( X2 - X1) ) + Y1
        //( X - X1 )
        var earlyConcat = highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue;

        if ((sliderHoursHighValue >= 24) && (sliderHoursCurrentValue <= 17)) {
          //adjustment will be wrong
    
          var hoursPadded = Math.floor(sliderHoursCurrentValue)+24;
        } else {

          if (Math.floor( sliderHoursCurrentValue) < 10) {
            var hoursPadded = "0"+Math.floor(sliderHoursCurrentValue);
          } else {
            var hoursPadded = Math.floor( sliderHoursCurrentValue);
          }
        }

        if (Math.floor( sliderMinsCurrentValue) < 10) {
          var minsPadded = "0"+Math.floor( sliderMinsCurrentValue);
        } else {
          var minsPadded = Math.floor( sliderMinsCurrentValue);
        }

        var hoursMinsRange = "" + hoursPadded+minsPadded;

        var firstCalc = Number(hoursMinsRange)-Number(earlyConcat);

        //( Y2 - Y1)
        var secCalc = Number(highAdjustment)-Number(lowAdjustment);

        //( X - X1 )( Y2 - Y1)
        var firstPartCalc = firstCalc*secCalc;

        //( X2 - X1) ) 
        var laterTimeHM = Number(highWaterLateTimeHoursValue+highWaterLateTimeMinsValue);
        var earlierTimeHM = Number(highWaterEarlyTimeHoursValue+highWaterEarlyTimeMinsValue);
        var thirdCalc = laterTimeHM-earlierTimeHM;

        //( X - X1 )( Y2 - Y1) / ( X2 - X1)
        var secPartCalc = firstPartCalc/thirdCalc;

        //All+Y1
        var totalCalc = Number(lowAdjustment)+Number(secPartCalc);
      
        var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (2 || -1) + '})?');
        var truncated = totalCalc.toString().match(re)[0];

        setFinalCalculation(truncated);
        if (truncated<0) {

          var hourAdjustments = Math.floor(truncated/60);
          var adjustedHours = Math.floor(sliderHoursCurrentValue)+hourAdjustments;

          adjustedMins = Math.floor(truncated-(hourAdjustments*60));

          var overallMins = (Number(sliderMinsCurrentValue)+Number(adjustedMins));

          if (overallMins >= 60) {
            adjustedHours = adjustedHours+1;
            adjustMins = overallMins-60;
          } else {
            adjustMins = overallMins;
          }
        

          setFinalAdjustedTime(zeroPad(adjustedHours)+":"+zeroPad(adjustMins));
        } else {

          var hourAdjustments = Math.floor(truncated/60);
          var adjustedHours = Math.floor(sliderHoursCurrentValue)+hourAdjustments;

          adjustedMins = Math.floor(truncated-(hourAdjustments*60));

          var overallMins = (Number(sliderMinsCurrentValue)+Number(adjustedMins));

          if (overallMins >= 60) {
            adjustedHours = adjustedHours+1;
            adjustMins = overallMins-60;
          } else {
            adjustMins = overallMins;
          }

          setFinalAdjustedTime(zeroPad(adjustedHours)+":"+zeroPad(adjustMins));
        } 
        
      }
    
  }

  function tryHeightCalc(event) {
    if ((lowHeightAdjustment) && (highHeightAdjustment)) {

      setHeightSlider(true);

      if (typeof sliderHeightCurrentValue != 'undefined') {
        setShowHeightFinalCalculation(true);

        //Interpolation: Y = ( ( X - X1 )( Y2 - Y1) / ( X2 - X1) ) + Y1
        //( X - X1 )
        var xx1 = sliderHeightCurrentValue - lowHeight;
        var y2y1 = lowHeightAdjustment - highHeightAdjustment;
        var x2x1 = highHeight - lowHeight;

        //( X - X1 )( Y2 - Y1)
        var firstHeightPart = xx1*y2y1;
        //( X2 - X1)
        var secondHeightPart = x2x1;
        //( X - X1 )( Y2 - Y1) / ( X2 - X1)
        var thirdHeightPart = firstHeightPart/secondHeightPart;
        //All + Y1
        var fourthHeightPart = Number(thirdHeightPart)+Number(highHeightAdjustment);

        var totalHeightCalc = fourthHeightPart;
        
        var reHeight = new RegExp('^-?\\d+(?:\.\\d{0,' + (2 || -1) + '})?');
        var heightTruncated = totalHeightCalc.toString().match(reHeight)[0];

        setFinalHeightCalculation(heightTruncated);

        setFinalAdjustedHeight(Number(heightTruncated)+Number(sliderHeightCurrentValue));
      }
    }
  }

  return (
    <SafeAreaView style={customStyles.container}>
      <View>
        <>
          <Text style={customStyles.headerText}>Secondary Port Calculator</Text>
          <Text style={customStyles.byBrand}>by iPowerboat</Text>
        </>
        <>
          <Tab
            value={index}
            onChange={(e) => setIndex(e)}
            indicatorStyle={{
              backgroundColor: 'white',
            }}
            variant="primary"
          >
            <Tab.Item
              title="Times"
              titleStyle={{ fontSize: 12 }}
              containerStyle={(active) => ({
                backgroundColor: active ? undefined : "black",
              })}
            />
            <Tab.Item
              title="Height"
              titleStyle={{ fontSize: 12 }}
              containerStyle={(active) => ({
                backgroundColor: active ? undefined : "black",
              })}
            />
          </Tab>

          <TabView value={index} onChange={setIndex} animationType="spring">
            <TabView.Item style={customStyles.tabView}>
                <View>
                  <View style={{flexDirection: 'row', marginTop: 50}}>
                    <Text style={customStyles.label}>Times:</Text>
                    <TextInput value={highWaterEarlyTimeHoursValue} ref={ref_input1} autoFocus={true} keyboardType="numeric" returnKeyType="done" style={customStyles.timeInput} id="highWaterEarlyTimeHours" maxLength={2} name="highWaterEarlyTimeHours" onChangeText={(value)=>{if (value.length === 2) {ref_input2.current.focus()};sethighWaterEarlyTimeHoursValue(value)}} onBlur={showHideRange} />
                    <Text style={{paddingTop: 15}}>:</Text>
                    <TextInput value={highWaterEarlyTimeMinsValue} ref={ref_input2} keyboardType="numeric" returnKeyType="done" style={customStyles.timeInput} id="highWaterEarlyTimeMins"  maxLength={2} name="highWaterEarlyTimeMins" onChangeText={(value)=>{if (value.length === 2) {ref_input3.current.focus()};sethighWaterEarlyTimeMinsValue(value)}} onBlur={showHideRange} />

                    <TextInput value={highWaterLateTimeHoursValue} ref={ref_input3} keyboardType="numeric" returnKeyType="done" style={customStyles.timeInputLM} id="highWaterLateTimeHours"  maxLength={2} name="highWaterLateTimeHours" onChangeText={(value)=>{if (value.length === 2) {ref_input4.current.focus()};sethighWaterLateTimeHoursValue(value)}} onBlur={showHideRange} />
                    <Text style={{paddingTop: 15}}>:</Text>
                    <TextInput value={highWaterLateTimeMinsValue} ref={ref_input4} keyboardType="numeric" returnKeyType="done" style={customStyles.timeInput} id="highWaterLateTimeMins"  maxLength={2} name="highWaterLateTimeMins" onChangeText={(value)=>sethighWaterLateTimeMinsValue(value)} onBlur={showHideRange} />
                  </View>
                  <>
                    {showAdjustment ? 
                    <View style={{flexDirection: 'row', marginTop: 50}}>
                      <Text style={{marginLeft: 30, marginRight: 20, paddingTop: 13}}>Adjustment:</Text>
                      <TextInput ref={ref_input5} id="lowAdjustment" keyboardType="numeric" returnKeyType="done" style={customStyles.adjInput} name="lowAdjustment" onChangeText={(value)=>setlowAdjustmentValue(value)}  onBlur={showHourMinSlider} />
                      <TextInput ref={ref_input6} id="highAdjustment" keyboardType="numeric" returnKeyType="done" style={customStyles.adjInputLM} name="highAdjustment" onChangeText={(value)=>sethighAdjustmentsValue(value)}  onBlur={showHourMinSlider} />
                    </View> : null}
                  </>
                  <>
                    {showSlider ?
                    <><Text style={{marginLeft: 30, marginRight: 20, paddingTop: 23}}>Set Exact Time</Text>
                    <Text style={{marginLeft: 40, paddingTop: 13}}>Hours</Text>
                    <React.Fragment style={customStyles.sliderContainer}>
                      <Slider
                      animateTransitions
                      thumbStyle={customStyles.thumb}
                      trackStyle={customStyles.track}
                      step={'1'}
                      minimumValue={sliderHoursLowValue}
                      maximumValue={sliderHoursHighValue}
                      minimumTrackTintColor="#606070"
                      maximumTrackTintColor="#d3d3d3"
                      value={sliderHoursValue}
                      onValueChange={(value)=>sliderHourChange(value)}
                      
                       />
                  </React.Fragment>
                  <Text style={{marginLeft: 30, marginRight: 20, paddingTop: 13}}>Mins</Text>
                  <React.Fragment style={customStyles.sliderContainer}>
                      <Slider
                      animateTransitions
                      thumbStyle={customStyles.thumb}
                      trackStyle={customStyles.track}
                      minimumValue={0}
                      maximumValue={59}
                      minimumTrackTintColor="#606070"
                      maximumTrackTintColor="#d3d3d3"
                      value={sliderMinsValue}
                      onValueChange={(value)=>sliderMinsChange(value)}
                      
                       />
                   </React.Fragment>
                  <Text style={ { fontWeight: 'bold', fontSize: 15, width: 370, height: 40, textAlign: 'center', marginLeft: 30, marginRight: 20, paddingTop: 13 } }>
                  Standard Port: {sliderHoursCurrentValue}:{sliderMinsCurrentValue}
                </Text></>: null}
                  </>
                  <>
                  {showFinalCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 80, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#6699cc' } }>
                  Adjustment: {Math.floor(finalCalculation)} mins </Text>: null}
                  </>
                  <>
                  {showFinalCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 120, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#4172a3' } }>Secondary Port:{'\n'}
                  {finalAdjustedTime} </Text>: null}
                  </>
                  
                  
                
                </View>
            </TabView.Item>
            <TabView.Item>
            <View>
                <View style={{flexDirection: 'row', marginTop: 50}}>
                  <Text style={{paddingTop: 13, width: 100, height: 50, color: 'black', marginLeft: 30, }}>Height:</Text>
                  <TextInput keyboardType="numeric" returnKeyType="done" style={customStyles.timeInput} id="lowHeight" name="lowHeight" onChangeText={(value)=>setLowHeight(value)} onBlur={showHideHeightRange} />
                  <Text style={{paddingTop: 15, height: 50, color: 'black'}}> m</Text>
                

                  <TextInput keyboardType="numeric" returnKeyType="done" style={customStyles.timeInputLM} id="highHeight"  name="highHeight" onChangeText={(value)=>setHighHeight(value)} onBlur={showHideHeightRange} />
                  <Text style={{paddingTop: 15, height: 50, color: 'black'}}> m</Text>
                
                </View>
              <>
                  {showHeightAdjustment ? 
                  <View style={{flexDirection: 'row', marginTop: 50, marginBottom: 30}}>
                    <Text style={{marginLeft: 30, width: 100, height: 50, marginTop: 20, color: 'black', paddingTop: 13}}>Adjustment:</Text>
                    <TextInput id="lowHeightAdjustment" keyboardType="numeric" returnKeyType="done" style={customStyles.adjInput} name="lowHeightAdjustment" onChangeText={(value)=>setlowHeightAdjustmentValue(value)}  onBlur={tryHeightCalc} />
                    <TextInput id="highHeightAdjustment" keyboardType="numeric" returnKeyType="done" style={customStyles.adjInputLM} name="highHeightAdjustment" onChangeText={(value)=>sethighHeightAdjustmentsValue(value)}  onBlur={tryHeightCalc} />
                  </View> : null}
                </>
                <>
                  {showHeightSlider ?
                  <><Text style={{marginLeft: 30, marginRight: 20, paddingTop: 23, marginTop: 30}}>Set Exact Height</Text>
                  
                  <Slider
                  animateTransitions
                  thumbStyle={customStyles.thumb}
                  trackStyle={customStyles.track}
                  step={'0.1'}
                  minimumValue={sliderHeightLowValue}
                  maximumValue={(sliderHeightHighValue)}
                  minimumTrackTintColor="#606070"
                  maximumTrackTintColor="#d3d3d3"
                  value={sliderHeightValue}
                  onValueChange={(value)=>sliderHeightChange(value)}
                  
                />
                <Text style={ { fontWeight: 'bold', fontSize: 15, width: 370, height: 40, textAlign: 'center', marginLeft: 30, marginRight: 20, paddingTop: 13 } }>
                Standard Port: {sliderHeightCurrentValue.toFixed(1)} m
              </Text></>: null}
                </>
                <>
                {showFinalHeightCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 80, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#6699cc' } }>
                {finalHeightCalculation} m </Text>: null}
                </>
                <>
                  {showFinalHeightCalculation ? <Text style={ { color: 'white', fontSize: 35, width: 370, height: 120, textAlign: 'center', marginLeft: 30, marginTop: 10, marginRight: 20, paddingTop: 13,backgroundColor: '#4172a3' } }>Secondary Port:{'\n'}
                  {finalAdjustedHeight.toFixed(2)} </Text>: null}
                </>
                
                
              
              </View>
            </TabView.Item>
          </TabView>
          </>
        </View>
      </SafeAreaView>
  );
}

const customStyles = StyleSheet.create({

    headerText: {
      color: 'white', 
      width: '100%', 
      backgroundColor: '#00c2fb', 
      height: 100, 
      paddingTop: 60, 
      alignContent: 'center', 
      textAlign: 'center', 
      fontFamily: 'HelveticaNeue',
      fontSize: 20,
    },

    byBrand: {
      color: 'white', 
      width: '100%', 
      backgroundColor: '#00c2fb', 
      paddingBottom: 20,
      alignContent: 'center', 
      textAlign: 'center', 
      fontFamily: 'HelveticaNeue',
      fontSize: 15,
    },

    tabView: {
      height: 800, 
      width: '100%',
    },

    label: {
      marginLeft: 30, 
      marginRight: 20, 
      paddingTop: 25
    },

    headerFooterStyle: {
      width: '100%',
      height: 45,
      backgroundColor: 'red',
    },
  
    timeInput: {
      width: 60,
      height: 70,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      backgroundColor: 'lightgrey',
      fontSize: 25,
      textAlign: 'center',
      borderColor: 'lightslategrey',
      borderWidth: 2
    },
  
    timeInputLM: {
      width: 60,
      height: 70,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginLeft: 50,
      backgroundColor: 'lightgrey',
      fontSize: 25,
      textAlign: 'center',
      borderColor: 'lightslategrey',
      borderWidth: 2
    },
  
    adjInput: {
      width: 60,
      height: 70,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginRight: 15,
      backgroundColor: 'lightgrey',
      fontSize: 25,
      textAlign: 'center', 
      borderColor: 'lightslategrey',
      borderWidth: 2
    },
  
    adjInputLM: {
      width: 60,
      height: 70,
      padding: 5,
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: 10,
      marginLeft: 50,
      backgroundColor: 'lightgrey',
      fontSize: 25,
      textAlign: 'center',
      borderColor: 'lightslategrey',
      borderWidth: 2
    },

    thumb: {
        backgroundColor: '#eaeaea',
        borderColor: '#9a9a9a',
        borderRadius: 2,
        borderWidth: 1,
        height: 20,
        width: 20,
    },
    track: {
        backgroundColor: 'white',
        borderColor: '#9a9a9a',
        borderRadius: 2,
        borderWidth: 1,
        height: 14,
        marginLeft: 10,
        marginRight: 10,
    },
    sliderContainer: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'center',
        backgroundColor: 'blue'
    }
  
  });